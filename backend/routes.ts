import express from "express";
import { userRoutes } from "./routers/userRoutes";
import { reportRoutes } from "./routers/reportRoutes";
import { repairRoutes } from "./routers/repairRoutes";
import { parkingRoutes } from "./routers/parkingRoutes";
import { contactListRoutes } from "./routers/contactListRoutes";
import { adminRoutes } from "./routers/adminRoutes";

export const routes = express.Router();
routes.use("/users", userRoutes); // localhost:8080/api/user
routes.use("/report", reportRoutes);
routes.use("/repair", repairRoutes);
routes.use("/parking", parkingRoutes);
routes.use("/tow", contactListRoutes);
routes.use("/admin", adminRoutes);
