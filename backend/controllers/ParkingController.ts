import { Request, Response } from "express";
import { ParkingService } from "../services/ParkingService";
import { logger } from "../utils/logger";

export class ParkingController {
  private readonly tag = "ParkingController";
  constructor(private parkingService: ParkingService) {}

  getPosted = async (req: Request, res: Response) => {
    try {
      const items = await this.parkingService.getPosted();
      res.json({ items });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getUserById = async (req: Request, res: Response) => {
    try {
      const userEmail = await this.parkingService.getUserById(req.body.id);
      res.json(userEmail);
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
