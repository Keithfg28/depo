import { UserService } from "../services/UserService";
import { Request, Response } from "express";
import { checkPassword, hashPassword } from "../utils/hash";
import { User } from "../services/models";
import jwtSimple from "jwt-simple";
import jwt from "../utils/jwt";
import fetch from "node-fetch";

const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID);

export class UserController {
  constructor(private userService: UserService) {}

  login = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      if (
        email === undefined ||
        email === null ||
        password === undefined ||
        password === null
      ) {
        res.status(400).json({ message: "Email or Password is required" });
      }
      const user: User | undefined = await this.userService.getUserByUsername(
        email
      );
      if (!user) {
        res.status(401).json({ message: "Wrong username" });
        return;
      }
      if (user && !(await checkPassword(password, user.password))) {
        res.status(401).json({ message: "Wrong password" });
        return;
      }

      const payload = { id: user.id, email: user.email, role: user.role };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json(token);
      return;
    } catch (e) {
      res.status(500).json({ message: e });
      return;
    }
  };

  loginGoogle = async (req: Request, res: Response) => {
    try {
      const { accessToken } = req.body;
      if (!accessToken) {
        res.status(401).json({ message: "Wrong Access Token!" });
        return;
      }
      const ticket = await client.verifyIdToken({
        idToken: accessToken,
        audience: process.env.CLIENT_ID,
      });
      const payload = ticket.getPayload();
      let email = payload.email;
      let user = await this.userService.getUserByUsername(email);
      const password = hashPassword("szjrhgkdsjf");
      if (!user) {
        user = await this.userService.createUser(email, await password);
      }

      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json(token);
      return;
    } catch (e) {
      res.status(500).json({ message: e });
      return;
    }

    res.json("accessToken");
  };

  loginFacebook = async (req: Request, res: Response) => {
    try {
      const accessToken = req.body.accessToken;
      if (!accessToken) {
        res.status(401).json({ message: "Wrong Access Token!" });
        return;
      }
      const url = `https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`;
      const fetchRes = await fetch(url);
      const fetchResult: any = await fetchRes.json();
      const email = fetchResult.email;
      const password = hashPassword("skrjgkfsfdb");
      let user = await this.userService.getUserByUsername(email);
      if (!user) {
        user = await this.userService.createUser(email, await password);
      }
      const payload = { id: user!.id, email: user!.email };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json(token);
      return;
    } catch (e) {
      res.status(500).json({ message: e });
      return;
    }

    res.json("loginFacebook");
  };

  createUser = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      if (
        email === undefined ||
        email === null ||
        password === undefined ||
        password === null
      ) {
        res.status(400).json({ message: "Email or Password is required" });
      }
      let user: User | undefined = await this.userService.getUserByUsername(
        email
      );

      if (!user) {
        let hashedPassword = await hashPassword(password);
        user = await this.userService.createUser(email, hashedPassword);
        const payload = { id: user!.id, email: email };
        const token = jwtSimple.encode(payload, jwt.jwtSecret);
        res.status(200).json({ token, message: "create account successful" });
        return;
      } else {
        res.status(401).json({ message: "Account have been registered" });
        return;
      }
    } catch (e) {
      res.status(500).json({ message: e });
      return;
    }
  };

  changePassword = async (req: Request, res: Response) => {
    try {
      const { email, currentPassword, newPassword } = req.body;
      let user: User | undefined = await this.userService.getUserByUsername(
        email
      );
      let match = await checkPassword(currentPassword, user.password);
      if (user && match === true) {
        let password = await hashPassword(newPassword);
        await this.userService.changePassword(user.email, password);
        res.status(200).json({ message: "change Password backend" });
      } else {
        res
          .status(401)
          .json({ message: "Password not correct, change password failed." });
        return;
      }
    } catch (e) {
      res.status(500).json({ message: e });

      return;
    }
  };

  getReportedItems = async (req: Request, res: Response) => {
    try {
      const { id } = req.body;
      const items = await this.userService.getReportedItems(id);

      res.json({ message: "report items", items });
    } catch (err) {
      res.status(500).json({ message: "internal server error" });
    }
  };
}
