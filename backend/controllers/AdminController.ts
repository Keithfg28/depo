import { Request, Response } from "express";
import { AdminService } from "../services/AdminService";
import { logger } from "../utils/logger";

export class AdminController {
  private readonly tag = "AdminController";
  constructor(private AdminService: AdminService) {}

  getPosted = async (req: Request, res: Response) => {
    try {
      const items = await this.AdminService.getPosted();
      res.json({ items });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getPending = async (req: Request, res: Response) => {
    try {
      const items = await this.AdminService.getPending();
      res.json({ items });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };
  getRemovedPosted = async (req: Request, res: Response) => {
    try {
      const itemId = await this.AdminService.removeItem(req.body.id);
      res.json({ message: "removed post", itemId });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  postToPublic = async (req: Request, res: Response) => {
    try {
      const itemId = await this.AdminService.postToPublic(req.body.id);
      res.json({ message: "post to public", itemId });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getEdited = async (req: Request, res: Response) => {
    try {
      const hourly_rent = await parseInt(req.body.data.hourly_rent);
      const day_rent = await parseInt(req.body.data.day_rent);
      const night_rent = await parseInt(req.body.data.night_rent);
      const place = req.body.data.place;
      const location = req.body.data.location;
      const id = req.body.id;
      const remark = req.body.data.remark;
      const editedItemId = await this.AdminService.getEdited(
        id,
        hourly_rent,
        day_rent,
        night_rent,
        place,
        location,
        remark
      );
      res.json({ message: "post to public", editedItemId });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getUserById = async (req: Request, res: Response) => {
    try {
      const userEmail = await this.AdminService.getUserById(req.body.id);
      res.json(userEmail);
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
