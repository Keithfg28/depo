export interface User {
  id: number;
  email: string;
  role: string;
  point: number;
  password: string;
}

export interface ReportItem {
  location: string;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  image: string;
  remark: string;
}
export interface RepairItem {
  name: string;
  location: string;
  phone: number;
  district: string;
  status: string;
}

export interface ParkingItem {
  id: number
  place: string
  location: string
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  image: string;
  remark: string;
  users_id: number;
  post: boolean;
}

export interface ContactListItem {
  id: number;
  name: string;
  phone: number;
  language: string;
  second_language: string;
  status: string;
}

interface ReqUser {
  id: number
  email: string
}
declare global {
  namespace Express {
    interface Request {
      user?: ReqUser
    }
  }
}
