import { Knex } from "knex";
import { RepairItem } from "./models";

export class RepairService {
  constructor(private knex: Knex) {}

  async getAll() {
    const result = await this.knex<RepairItem>("car_repair")
      .select("*")
      .orderBy("id", "desc");
    return result;
  }

  async getById(id: number) {
    const result = await this.knex<RepairItem>("car_repair")
      .select("id", "description", "is_checked")
      .where("user_id", id)
      .orderBy("id", "desc");
    return result;
  }
}
