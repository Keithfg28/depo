import { Knex } from "knex";
import { ReportItem } from "./models";

export class ReportService {
  constructor(private knex: Knex) {}

  async getAll() {
    const result = await this.knex<ReportItem>("parking_place")
      .select("*")
      .orderBy("id", "desc");
    return result;
  }

  async getById(id: number) {
    const result = await this.knex<ReportItem>("parking_place")
      .select("id", "description", "is_checked")
      .where("user_id", id)
      .orderBy("id", "desc");
    return result;
  }

  async createReportData(data: ReportItem[]) {
    const result = await this.knex("parking_place").insert(data);
    return result;
  }
}
