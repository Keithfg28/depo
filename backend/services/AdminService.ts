import { Knex } from "knex";
import { ParkingItem, User } from "./models";

export class AdminService {
  constructor(private knex: Knex) {}

  async getPosted() {
    const result = await this.knex<ParkingItem>("parking_place")
      .select("*")
      .where({ post: true })
      .orderBy("id", "desc");
    return result;
  }

  async getPending() {
    const result = await this.knex<ParkingItem>("parking_place")
      .select("*")
      .where({ post: false })
      .orderBy("id", "desc");

    return result;
  }

  async getById(id: number) {
    const result = await this.knex<ParkingItem>("parking_place")
      .select("id", "description", "is_checked")
      .where("user_id", id)
      .orderBy("id", "desc");
    return result;
  }

  async removeItem(id: number) {
    const result = await this.knex<ParkingItem>("parking_place")
      .update("post", false)
      .where("id", id);
    return result;
  }

  async postToPublic(id: number) {
    const result = await this.knex<ParkingItem>("parking_place")
      .update("post", true)
      .where("id", id);
    return result;
  }

  async getEdited(
    id: number,
    hourly_rent: number,
    day_rent: number,
    night_rent: number,
    place: string,
    location: string,
    remark: string
  ) {
    const result = await this.knex<ParkingItem>("parking_place")
      .update("hourly_rent", hourly_rent)
      .update("day_rent", day_rent)
      .update("night_rent", night_rent)
      .update("place", place)
      .update("location", location)
      .update("remark", remark)
      .where("id", id);
    return result;
  }

  async getUserById(id: number) {
    const user = await this.knex<User>("users").select("email").where({ id });
    return user;
  }
}
