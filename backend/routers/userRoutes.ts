import express from "express";
import { UserService } from "../services/UserService";
import { UserController } from "../controllers/UserController";
import { knex } from "../app";
export const userService = new UserService(knex);
const userController = new UserController(userService);

export const userRoutes = express.Router();
userRoutes.post("/login", userController.login); //localhost:8080/api/users/login [POST]
userRoutes.post("/loginFacebook", userController.loginFacebook); //localhost:8080/api/users/loginFacebook [POST]
userRoutes.post("/loginGoogle", userController.loginGoogle); //localhost:8080/api/users/loginGoogle [POST]
userRoutes.post("/createUser", userController.createUser); //localhost:8080/api/users/creatUser [POST]
userRoutes.put("/changePassword", userController.changePassword); //localhost:8080/api/users/changePassword [PUT]
userRoutes.post("/reported", userController.getReportedItems); //localhost:8080/api/users/reported [POST]
