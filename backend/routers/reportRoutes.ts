import express from "express";
import { ReportService } from "../services/ReportService";
import { ReportController } from "../controllers/ReportController";
import { knex } from "../app";

const reportService = new ReportService(knex);
const reportController = new ReportController(reportService);

export const reportRoutes = express.Router();

reportRoutes.get("/", reportController.getAll); //localhost:8080/api/report [GET]
reportRoutes.post("/", reportController.createReportData)
