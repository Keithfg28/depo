import express from "express";
import { ParkingService } from "../services/ParkingService";
import { ParkingController } from "../controllers/ParkingController";
import { knex } from "../app";

const parkingService = new ParkingService(knex);
const parkingController = new ParkingController(parkingService);

export const parkingRoutes = express.Router();

parkingRoutes.get("/", parkingController.getPosted); //localhost:8080/api/repair [GET]
parkingRoutes.post("/getUser", parkingController.getUserById); //localhost:8080/api/admin/getUser [POST]
