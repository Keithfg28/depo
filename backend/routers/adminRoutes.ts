import express from "express";

import { knex } from "../app";
import { AdminController } from "../controllers/AdminController";
import { AdminService } from "../services/AdminService";

const adminService = new AdminService(knex);
const adminController = new AdminController(adminService);

export const adminRoutes = express.Router();

adminRoutes.get("/post", adminController.getPosted); //localhost:8080/api/admin/post [GET]
adminRoutes.get("/pending", adminController.getPending); //localhost:8080/api/admin/pending [GET]
adminRoutes.delete("/post", adminController.getRemovedPosted); //localhost:8080/api/admin/post [GET]
adminRoutes.put("/pending", adminController.postToPublic); //localhost:8080/api/admin/pending [PUT]
adminRoutes.put("/edit", adminController.getEdited); //localhost:8080/api/admin/edit [PUT]
adminRoutes.post("/getUser", adminController.getUserById); //localhost:8080/api/admin/getUser [POST]
