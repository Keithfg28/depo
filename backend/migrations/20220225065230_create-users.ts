import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("users", (table) => {
    table.increments();
    table.string("email").unique();
    table.string("role").defaultTo("member");
    table.integer("point").defaultTo("0");
    table.string("password");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("parking_place", (table) => {
    table.increments();
    table.string("place");
    table.string("location");
    table.string("place_id").unique();
    table.decimal("lat", 14, 2);
    table.decimal("lng", 14, 2);
    table.integer("hourly_rent").defaultTo("000");
    table.integer("day_rent").defaultTo("000");
    table.integer("night_rent").defaultTo("000");
    table.boolean("isIndoor");
    table.string("image");
    table.text("remark");
    table.string("users_id").notNullable;
    table.boolean("post").defaultTo(false);
    table.timestamps(false, true);
  });
  await knex.schema.createTable("car_towing", (table) => {
    table.increments();
    table.string("name");
    table.integer("phone");
    table.string("language").defaultTo("");
    table.string("second_language").defaultTo("");
    table.string("status").defaultTo("available");
    table.timestamps(false, true);
  });
  await knex.schema.createTable("car_repair", (table) => {
    table.increments();
    table.string("name");
    table.string("location");
    table.integer("phone");
    table.string("place_id").unique();
    table.string("district");
    table.string("status").defaultTo("available");
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("parking_place");
  await knex.schema.dropTableIfExists("car_towing");
  await knex.schema.dropTableIfExists("car_repair");
  await knex.schema.dropTableIfExists("users");
}
