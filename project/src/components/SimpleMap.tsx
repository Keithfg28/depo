import "../css/simpleMap.css";
import apiKey from "../key";
import GoogleMapReact from "google-map-react";
import { CSSProperties, useCallback, useRef, useState } from "react";
import { IRootState } from "../redux/store";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getRepairItemThunk } from "../redux/repair/thunks";
import { getPostedParkingItemThunk } from "../redux/parking/thunks";
import myLocationIcon from "../asset/image/Map_Location_Icon_SVG_emlhbv.png";
import markerIcon1 from "../asset/image/123.png";
import markerIcon2 from "../asset/image/456.png";
import { Button, Container } from "react-bootstrap";
import {
  faLocationDot,
  faLocationCrosshairs,
  faRoute,
  faSatellite,
  faScrewdriverWrench,
} from "@fortawesome/free-solid-svg-icons";
import {
  SetMapApi,
  SetMapApiLoaded,
  SetMapInstance,
  UpdateParkingItemsGeometry,
} from "../redux/parking/actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { debounce } from "lodash";
import { fetchGetUserById } from "../api/parking";
import {
  myPosition,
  parkingPlacesState,
  repairPlacesState,
  repairMarkers,
  placeMarkers,
  defaultProps,
} from "../redux/parking/state";

const MyPositionMarker = (props: myPosition) => (
  <div>
    <img style={{ height: "30px", width: "30px" }} src={myLocationIcon} />
  </div>
);

export default function SimpleMap() {
  const [myPosition, setMyPosition] = useState({
    lat: 22.3201763,
    lng: 114.1526038,
  });
  let inputRef = useRef<any>(null);
  const [inputText, setInputText] = useState<string>("");
  const [mapInstance, setMapInstance] = useState<any>();
  const [mapType, setMapType] = useState<string>("roadmap");
  const [repairPlaces, setRepairPlaces] = useState<repairPlacesState[]>([]);
  const [parkingPlaces, setParkingPlaces] = useState<parkingPlacesState[]>([]);
  const [isLocating, setIsLocating] = useState<boolean>(false);
  const [directionsDisplay, setDirectionsDisplay] = useState<any>(null);
  const [routesMarkers, setRoutesMarkers] = useState<any[]>([]);
  const [placeMarkers, setPlaceMarkers] = useState<placeMarkers[]>([]);
  const [repairMarkers, setRepairMarkers] = useState<repairMarkers[]>([]);
  const [isClicked, setIsClicked] = useState<boolean>(false);
  const [autocompleteResults, setAutocompleteResults] = useState<any[]>([]);
  const [currentCenter, setCurrentCenter] = useState<myPosition>(myPosition);
  const repairItems = useSelector(
    (state: IRootState) => state.repair.repairItems
  );
  const parkingItems = useSelector(
    (state: IRootState) => state.parking.parkingItems
  );
  const mapApi = useSelector((state: IRootState) => state.parking.mapApi);
  const mapApiLoaded = useSelector(
    (state: IRootState) => state.parking.mapApiLoaded
  );
  const dispatch = useDispatch();
  useEffect(() => {
    getMyLocation();
    dispatch(getRepairItemThunk());
    dispatch(getPostedParkingItemThunk());
  }, [mapApiLoaded]);
  useEffect(() => {
    handleAutocomplete();
  }, [inputText]);
  useEffect(() => {
    if (directionsDisplay === null) {
      if (mapApi) {
        setDirectionsDisplay(new mapApi.DirectionsRenderer());
      }
    }
  }, [mapApiLoaded]);

  const clearPlaceMarkers = (markers: any) => {
    for (let marker of markers) {
      marker.setMap(null);
    }
    if (markers == repairMarkers) {
      setRepairMarkers([]);
    } else {
      setPlaceMarkers([]);
    }
  };

  const handleApiLoaded = (map: any, maps: any) => {
    setMapInstance(map);
    dispatch(SetMapApi(maps));
    dispatch(SetMapApiLoaded());
  };

  const handleMapTypeId = (type: string) => {
    setMapType(type);
  };

  const getMyLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        setMyPosition({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
        setCurrentCenter(myPosition);
      });
    } else {
      alert("定位失敗");
    }
  };
  const handleInput = () => {
    if (inputRef) {
      setInputText(inputRef.current.value);
    }
  };
  const handleAutocomplete = () => {
    if (mapApiLoaded) {
      const service = new mapApi.places.AutocompleteService();
      const request = {
        input: inputText,
      };
      service.getPlacePredictions(request, (results: any, status: any) => {
        if (status === mapApi.places.PlacesServiceStatus.OK) {
          setAutocompleteResults(results);
        }
      });
    }
  };

  const handleClickToChangeMyPosition = (e: any) => {
    const placeId = e.target.getAttribute("id");

    const service = new mapApi.places.PlacesService(mapInstance);
    const request = {
      placeId,
      fields: ["geometry"],
    };

    service.getDetails(request, (results: any, status: any) => {
      if (status === mapApi.places.PlacesServiceStatus.OK) {
        const newPosition = {
          lat: results.geometry.location.lat(),
          lng: results.geometry.location.lng(),
        };
        setCurrentCenter(newPosition);
        setAutocompleteResults([]);
        inputRef.current.value = "";
      }
    });
  };

  const placeDirections = (lat: number | null, lng: number | null) => {
    const infoWindows: any[] = [];
    const directionsService = new mapApi.DirectionsService();
    for (let marker of routesMarkers) {
      marker.setMap(null);
    }
    setRoutesMarkers([]);
    directionsDisplay.setMap(mapInstance);
    const request = {
      origin: myPosition,
      destination: { lat, lng },
      travelMode: "DRIVING",
    };
    directionsService.route(request, function (result: any, status: any) {
      if (status == "OK") {
        const steps = result.routes[0].legs[0].steps;
        steps.forEach((e: any, i: any) => {
          routesMarkers[i] = new mapApi.Marker({
            position: {
              lat: e.start_location.lat(),
              lng: e.start_location.lng(),
            },
            map: mapInstance,
            label: { text: i + "", color: "#fff" },
          });
          infoWindows[i] = new mapApi.InfoWindow({
            content: e.instructions,
          });
          routesMarkers[i].addListener("click", function () {
            if (infoWindows[i].anchor) {
              infoWindows[i].close();
            } else {
              infoWindows[i].open(mapInstance, routesMarkers[i]);
            }
          });
        });
        directionsDisplay.setDirections(result);
      }
    });
  };

  const parkingMarkerDescription = useCallback(() => {
    const infoWindows: any[] = [];
    if (isClicked) {
      parkingPlaces.forEach(async (place: any, i: any) => {
        const result = await fetchGetUserById(place.users_id);
        const data = await result.json();
        const email = data[0].email;

        placeMarkers[i] = new mapApi.Marker({
          position: { lat: place.lat, lng: place.lng },
          map: mapInstance,
          icon: markerIcon1,
          label: {
            text: place.place,
            className: "marker parking",
          },
        });
        infoWindows[i] = new mapApi.InfoWindow({
          content:
            "<h1>" +
            place.place +
            "</h1>" +
            "<div class='marker-text'>" +
            place.location +
            "</div>" +
            (place.isIndoor === true
              ? "<div class='marker-text'>室內停車場</div>"
              : "<div class='marker-text'>露天停車場</div>") +
            "<div class='marker-text'>時租: $" +
            place.hourly_rent +
            "</div>" +
            (place.day_rent
              ? "<div class='marker-text'>日泊: $" + place.day_rent + "</div>"
              : "<div></div>") +
            (place.night_rent
              ? "<div class='marker-text'>夜泊: $" + place.night_rent + "</div>"
              : "<div></div>") +
            (place.remark
              ? "<div id='comment' class='marker-text'>會員備註:" +
                place.remark +
                "</div>"
              : "<div></div>") +
            (place.users_id
              ? "<div id='comment' class='marker-text'>由會員" +
                email +
                "提供資訊</div>"
              : "<div></div>") +
            (place.image
              ? `<img src=` + place.image + `/></img>`
              : `<img src=` + place.photos + `/></img>`) +
            '<div id="btn-container">' +
            '<div><input id="btn" type="button" value="前往" /></div>' +
            '<div><input id="back-btn" type="button" value="返回" /></div>' +
            "</div>",
        });

        placeMarkers[i].addListener("mouseover", function () {
          placeMarkers[i].setLabel({
            text: place.place,
            className: "marker-hover",
            color: "#fff",
          });
          placeMarkers[i].setZIndex(100);
        });
        placeMarkers[i].addListener("mouseout", function () {
          placeMarkers[i].setLabel({
            text: place.place,
            className: "marker",
            color: "#000000",
          });
        });
        placeMarkers[i].addListener("click", function () {
          if (infoWindows[i].anchor) {
            infoWindows[i].close();
          } else {
            infoWindows[i].open(mapInstance, placeMarkers[i]);
          }
        });
        infoWindows[i].addListener("domready", function () {
          const btn = document.getElementById("btn");
          const backBtn = document.getElementById("back-btn");
          btn!.addEventListener("click", function () {
            placeDirections(place.lat, place.lng);
          });
          backBtn!.addEventListener("click", function () {
            for (let marker of routesMarkers) {
              marker.setMap(null);
            }
            directionsDisplay.setMap(null);
            setRoutesMarkers([]);
          });
        });
      });
    } else {
      clearPlaceMarkers(placeMarkers);
    }
  }, [parkingPlaces]);

  useEffect(() => {
    parkingMarkerDescription();
  }, [parkingMarkerDescription]);

  const repairMarkerDescription = useCallback(() => {
    const infoWindows: any[] = [];
    if (repairMarkers.length == 0) {
      repairPlaces.forEach((place: any, i: any) => {
        repairMarkers[i] = new mapApi.Marker({
          position: { lat: place.lat, lng: place.lng },
          icon: markerIcon2,
          map: mapInstance,
          label: { text: place.name, color: "#000000", className: "repair" },
        });

        infoWindows[i] = new mapApi.InfoWindow({
          content:
            "<h1>" +
            place.name +
            "</h1>" +
            "<div class='marker-text'>" +
            place.location +
            "</div>" +
            (place.business_status === "OPERATIONAL"
              ? "<div id='opening' class='marker-text'>營業中</div>"
              : "<div id='close' class='marker-text'>休息中</div>") +
            "<div class='marker-text'>電話: " +
            place.phone +
            "</div>" +
            (place.image
              ? `<img src=` + place.image + `/></div>`
              : `<img src=` + place.photos + `/></div>`) +
            '<div id="btn-container">' +
            '<div><input id="btn" type="button" value="前往" /></div>' +
            '<div><input id="back-btn" type="button" value="返回" /></div>' +
            "</div>",
        });
        repairMarkers[i].addListener("mouseover", function () {
          repairMarkers[i].setLabel({
            text: place.name,
            className: "repair-marker-hover",
            color: "#fff",
          });
          repairMarkers[i].setZIndex(100);
        });
        repairMarkers[i].addListener("mouseout", function () {
          repairMarkers[i].setLabel({
            text: place.name,
            className: "repair",
            color: "#000000",
          });
        });
        repairMarkers[i].addListener("click", function () {
          if (infoWindows[i].anchor) {
            infoWindows[i].close();
          } else {
            infoWindows[i].open(mapInstance, repairMarkers[i]);
          }
        });
        infoWindows[i].addListener("domready", function () {
          const btn = document.getElementById("btn");
          const backBtn = document.getElementById("back-btn");
          btn!.addEventListener("click", function () {
            placeDirections(place.lat, place.lng);
          });
          backBtn!.addEventListener("click", function () {
            for (let marker of routesMarkers) {
              marker.setMap(null);
            }
            directionsDisplay.setMap(null);
            setRoutesMarkers([]);
          });
        });
      });
    } else {
      clearPlaceMarkers(repairMarkers);
    }
  }, [repairPlaces]);

  useEffect(() => {
    repairMarkerDescription();
  }, [repairMarkerDescription]);

  const findParkingLocation = () => {
    const parkingMap: any = {};
    setIsClicked(!isClicked);
    if (mapApiLoaded && mapApi) {
      if (isClicked) {
        clearPlaceMarkers(placeMarkers);
        return;
      }
      const service = new mapApi.places.PlacesService(mapInstance);
      for (let parkingItem of parkingItems) {
        const request = {
          placeId: parkingItem.place_id,
          fields: ["all"],
        };
        service.getDetails(request, (results: any, status: any) => {
          if (status === mapApi!.places.PlacesServiceStatus.OK) {
            if (results.place_id === parkingItem.place_id) {
              parkingMap[results.place_id] = parkingItem;
              parkingMap[results.place_id].lat =
                results.geometry.location.lat();
              parkingMap[results.place_id].lng =
                results.geometry.location.lng();
              parkingMap[results.place_id].photos = results.photos[0].getUrl();
              parkingMap[results.place_id].location = results.formatted_address;
            }
            const finalResults = [];
            for (let key in parkingMap) {
              finalResults.push(parkingMap[key]);
            }
            if (finalResults.length === parkingItems.length) {
              setParkingPlaces(finalResults);
            }
          }
        });
      }
    }
  };

  const findRepairLocation = () => {
    const repairMap: any = {};
    if (mapApiLoaded && mapApi) {
      const service = new mapApi.places.PlacesService(mapInstance);
      for (let repairItem of repairItems) {
        const request = {
          placeId: repairItem.place_id,
          fields: ["all"],
        };
        service.getDetails(request, (results: any, status: any) => {
          if (status === mapApi!.places.PlacesServiceStatus.OK) {
            if (results.place_id === repairItem.place_id) {
              repairMap[results.place_id] = repairItem;
              repairMap[results.place_id].lat = results.geometry.location.lat();
              repairMap[results.place_id].lng = results.geometry.location.lng();
              repairMap[results.place_id].photos = results.photos[0].getUrl();
              repairMap[results.place_id].location = results.formatted_address;
              repairMap[results.place_id].business_status =
                results.business_status;
            }
            const finalResults = [];
            for (let key in repairMap) {
              finalResults.push(repairMap[key]);
            }
            if (finalResults.length === repairItems.length) {
              setRepairPlaces(finalResults);
            }
          }
        });
      }
    }
  };

  return (
    <div style={{ height: "100vh", width: "100%", position: "fixed" }}>
      <div className="topContainer">
        <div className="fontASBtn" onClick={findParkingLocation}>
          <FontAwesomeIcon icon={faLocationDot} size="1x" />
          <br></br>停車場
        </div>
        <div className="fontASBtn" onClick={findRepairLocation}>
          <FontAwesomeIcon icon={faScrewdriverWrench} size="1x" />
          <br></br>車房
        </div>
        <div className="fontASBtn" onClick={getMyLocation}>
          <FontAwesomeIcon icon={faLocationCrosshairs} size="1x" />
          <br></br>我的位置
        </div>
        <div className="fontASBtn" onClick={() => handleMapTypeId("hybrid")}>
          <FontAwesomeIcon icon={faSatellite} size="1x" />
          <br></br>衛星
        </div>
        <div className="fontASBtn" onClick={() => handleMapTypeId("roadmap")}>
          <FontAwesomeIcon icon={faRoute} size="1x" />
          <br></br>路線
        </div>
      </div>
      <div className="search-box">
        <span className="search-box-title">地區搜索: </span>
        <input
          className="input-box"
          ref={inputRef}
          type="text"
          onChange={debounce(handleInput, 500)}
        />
        <div onClick={handleClickToChangeMyPosition}>
          {autocompleteResults && inputText
            ? autocompleteResults.map((item, id) => (
                <div className="map-auto-list" key={id} id={item.place_id}>
                  {item.description}
                </div>
              ))
            : null}
        </div>
      </div>
      <GoogleMapReact
        bootstrapURLKeys={{
          key: `${apiKey}`,
          language: "zh-HK",
          region: "zh-HK",
          libraries: ["places", "directions"],
        }}
        center={isLocating ? myPosition : currentCenter}
        zoom={isLocating ? 15 : defaultProps.zoom}
        options={{ mapTypeId: mapType }}
        defaultCenter={defaultProps.center}
        defaultZoom={defaultProps.zoom}
        yesIWantToUseGoogleMapApiInternals={true}
        onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
      >
        <MyPositionMarker lat={myPosition.lat} lng={myPosition.lng} />
      </GoogleMapReact>
    </div>
  );
}
