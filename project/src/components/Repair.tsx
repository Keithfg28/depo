import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getRepairItemThunk } from "../redux/repair/thunks";
import { IRootState } from "../redux/store";

export default function Repair() {
  const dispatch = useDispatch();
  const repairItems = useSelector(
    (state: IRootState) => state.repair.repairItems
  );

  useEffect(() => {
    dispatch(getRepairItemThunk());
  }, [dispatch]);

  return (
    <div>
      <h1>車房資料</h1>
      <div>
        {repairItems.map((repairItem, index) => (
          <div key={repairItem.id} className="repairItem">
            {index + 1}
            {repairItem.name}
            <br />
            {repairItem.location}
          </div>
        ))}
      </div>
    </div>
  );
}
