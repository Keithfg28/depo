import { useDispatch } from "react-redux";
import { Col, Container, Form, Row } from "reactstrap";
interface ReportedItem {
  id: number;
  place: string;
  location: string;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  image: string;
  remark: string;
  users_id: number;
  post: boolean;
}

export default function ReportedItems(props: ReportedItem) {
  const dispatch = useDispatch();
  return (
    <div>
      <Form>
        <Container className="parkingItems">
          <Row key={props.id}>
            <Col className="parkingItemData">
              <h2>
                {props.id} {props.place}
              </h2>
              <h2> {props.location}</h2>
              <h3>
                時租:${props.hourly_rent} 日租:${props.day_rent} 夜租:$
                {props.night_rent}{" "}
                {props.isIndoor ? "室內停車場" : "露天停車場"}
              </h3>
              <h3>資訊 {props.post ? "已上載" : "等待上載"}</h3>
              <h3>Remark: {props.remark}</h3>
              <h3>Photo:{props.image}</h3>
            </Col>
          </Row>
        </Container>
      </Form>
    </div>
  );
}
