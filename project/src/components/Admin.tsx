import { useEffect } from "react";
import { dispatch } from "react-hot-toast/dist/core/store";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../redux/auth/actions";
import { IRootState } from "../redux/store";
import { Accordion } from "react-bootstrap";
import "../css/admin.css";
import { getPendingParkingItemThunk, getPostedParkingItemThunk } from "../redux/admin/thunk";
import ParkingItems from "./ParkingItems";
import PendingItems from "./PendingItems";


export default function Admin() {
    const dispatch = useDispatch();
    const pendingParkingItems = useSelector(
        (state: IRootState) => state.admin.pendingItems
    );
    const postedParkingItems = useSelector(
        (state: IRootState) => state.admin.parkingItems
    );

    const clickLogout = () => {
        dispatch(logout());
    }

    useEffect(() => {
        dispatch(getPendingParkingItemThunk());
        dispatch(getPostedParkingItemThunk());
    }, []);



    return (
        <div>
            <h1>管理員帳戶</h1>
            <button onClick={clickLogout} className="logoutBtn">登出</button>
            <Accordion defaultActiveKey="0">
                <Accordion.Item eventKey="0">
                    <Accordion.Header className="adminHeader"> 等待上載資訊</Accordion.Header>
                    <Accordion.Body>
                        {pendingParkingItems.map((parkingItem, index) => (
                            <PendingItems
                                key={index}
                                id={parkingItem.id}
                                place={parkingItem.place}
                                location={parkingItem.location}
                                hourly_rent={parkingItem.hourly_rent}
                                day_rent={parkingItem.day_rent}
                                night_rent={parkingItem.night_rent}
                                isIndoor={parkingItem.isIndoor}
                                image={parkingItem.image}
                                remark={parkingItem.remark}
                                users_id={parkingItem.users_id}
                                post={parkingItem.post}
                            />
                        ))}

                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header className="adminHeader"> 已上載資訊</Accordion.Header>
                    <Accordion.Body>
                        {postedParkingItems.map((parkingItem, index) => (
                            <ParkingItems
                                key={index}
                                id={parkingItem.id}
                                place={parkingItem.place}
                                location={parkingItem.location}
                                hourly_rent={parkingItem.hourly_rent}
                                day_rent={parkingItem.day_rent}
                                night_rent={parkingItem.night_rent}
                                isIndoor={parkingItem.isIndoor}
                                image={parkingItem.image}
                                remark={parkingItem.remark}
                                users_id={parkingItem.users_id}
                                post={parkingItem.post}
                            />
                        ))}
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </div >

    );

}

