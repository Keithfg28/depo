import React from "react";
import Logo from "../asset/image/elook_white.gif"

class LogoComponent extends React.Component {
    render() {
      return (
          <img className="logo" src={Logo} alt="logo"/>
      );
    }
  }
  
  export default LogoComponent