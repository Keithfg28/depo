import React, {Fragment, useState} from "react";

const FileUpdate = () =>{
    const [file, setFile]= useState('');
    const [filename, setFilename] = useState('Choose File');
    const [uploadFile, setUploadedFile] = useState ({});

    const onChange = (e:any) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    }


}