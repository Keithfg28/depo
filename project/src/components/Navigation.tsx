import React from "react";
import { Nav, NavItem } from "reactstrap";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPenToSquare,
  faMapLocationDot,
  faScrewdriverWrench,
  faTruckPickup,
  faUserPlus,
  faUserMinus,
} from "@fortawesome/free-solid-svg-icons";
import "../css/navigation.css";
import { useSelector } from "react-redux";
import { IAuthState } from "../redux/auth/state";
import { IRootState } from "../redux/store";



const Navigation = (_props: any) => {
  const isAuthenticate = useSelector(
    (state: IRootState) => state.auth.isAuthenticate
  );
  const isAdmin = useSelector(
    (state: IRootState) => state.auth.isAdmin
  );
  const tabs = [
    {
      route: "/report",
      icon: faPenToSquare,
      label: "報料",
    },
    {
      route: "/parking",
      icon: faMapLocationDot,
      label: "地圖",
    },
    {
      route: "/tow",
      icon: faTruckPickup,
      label: "拖車",
    },
    {
      route: isAuthenticate && isAdmin ? "/admin" : "/login/" || isAuthenticate ? "/account" : "/login/",
      icon: isAuthenticate ? faUserMinus : faUserPlus,
      label: isAuthenticate ? "帳戶" : "登入",
    },
  ];
  // console.log("isAuthenticate", isAuthenticate);
  // console.log("isAdmin", isAdmin);

  return (
    <div>
      <nav className="navbar fixed-bottom navbar-light" role="navigation">
        <Nav className="w-100">
          <div className=" d-flex flex-row justify-content-around w-100">
            {tabs.map((tab, index) => (
              <NavItem key={`tab-${index}`}>
                <NavLink
                  to={tab.route}
                  className="nav-link"
                  activeClassName="active"
                >
                  <div className="row d-flex flex-column justify-content-center align-items-center">
                    <FontAwesomeIcon size="lg" icon={tab.icon} />
                    <div>{tab.label}</div>
                  </div>
                </NavLink>
              </NavItem>
            ))}
          </div>
        </Nav>
      </nav>
    </div>
  );
};

export default Navigation;
