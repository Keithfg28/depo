import { IAuthState } from "./auth/state";
import { IAuthAction } from "./auth/actions";
import { authReducers } from "./auth/reducers";
import { repairReducers } from "./repair/reducers";
import thunk, { ThunkDispatch } from "redux-thunk";
import {
  Action,
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
} from "redux";
import {
  CallHistoryMethodAction,
  connectRouter,
  routerMiddleware,
  RouterState,
} from "connected-react-router";
import { createBrowserHistory } from "history";
import { IRepairState } from "./repair/state";
import { IRepairAction } from "./repair/actions";
import { IContactListState } from "./tow/state";
import { IContactListAction } from "./tow/actions";
import { contactListReducers } from "./tow/reducers";
import { IParkingState } from "./parking/state";
import { parkingReducers } from "./parking/reducers";
import { IAdminState } from "./admin/state";
import { adminReducers } from "./admin/reducers";
import { IAdminAction } from "./admin/actions";
import { IReportState } from "./report/state";
import { IReportAction } from "./report/actions";
import { reportDataReducers } from "./report/reducers";
import { IAccState } from "./account/state";
import { accountReducers } from "./account/reducers";
import { IAccAction } from "./account/action";
import { IUtilState } from "./util/state";
import { utilReducers } from "./util/reducers";
import { IUtilAction } from "./util/action";

export const history = createBrowserHistory();
export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

export interface IRootState {
  auth: IAuthState;
  repair: IRepairState;
  tow: IContactListState;
  parking: IParkingState;
  admin: IAdminState;
  router: RouterState;
  report: IReportState;
  acc: IAccState;
  util: IUtilState;
}

type IRootAction =
  | IAuthAction
  | IRepairAction
  | CallHistoryMethodAction
  | IContactListAction
  | IAdminAction
  | IReportAction
  | IAccAction
  | IUtilAction;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers<IRootState>({
  auth: authReducers,
  repair: repairReducers,
  tow: contactListReducers,
  parking: parkingReducers,
  admin: adminReducers,
  router: connectRouter(history),
  report: reportDataReducers,
  acc: accountReducers,
  util: utilReducers,
});
const store = createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
  )
);

export default store;
