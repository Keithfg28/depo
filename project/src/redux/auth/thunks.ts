import { Dispatch } from "redux";
import {
  fetchFacebookLogin,
  fetchLogin,
  fetchCreateUser,
  fetchGoogleLogin,
  fetchChangePassword,
} from "../../api/auth";
import { IAuthAction, loginSuccess, loadToken } from "./actions";
import { CallHistoryMethodAction, push } from "connected-react-router";
import { InfoModal } from "../../components/InfoModal";
import { IUtilAction, SetModalContent } from "../util/action";

export function loginThunk(email: string, password: string) {
  return async (
    dispatch: Dispatch<IAuthAction | CallHistoryMethodAction | IUtilAction>
  ) => {
    const res = await fetchLogin(email, password);
    const result = await res.json();

    if (res.ok) {
      localStorage.setItem("token", result);
      dispatch(loginSuccess());
      dispatch(loadToken(result));
      dispatch(push("/report"));
    } else {
      dispatch(SetModalContent("密碼錯誤"));
    }
  };
}

export function loginGoogleThunk(accessToken: string) {
  return async (dispatch: Dispatch<IAuthAction | CallHistoryMethodAction>) => {
    const res = await fetchGoogleLogin(accessToken);
    const result = await res.json();
    if (res.ok) {
      dispatch(loginSuccess());
      dispatch(loadToken(result));
      dispatch(push("/report"));
    }
  };
}

export function loginFacebookThunk(accessToken: string) {
  return async (dispatch: Dispatch<IAuthAction | CallHistoryMethodAction>) => {
    const res = await fetchFacebookLogin(accessToken);
    const result = await res.json();
    if (res.ok) {
      localStorage.setItem("facebook token", result);
      dispatch(loginSuccess());
      dispatch(loadToken(result));
      dispatch(push("/report"));
    }
  };
}

export function createUserThunk(email: string, password: string) {
  return async (
    dispatch: Dispatch<IAuthAction | CallHistoryMethodAction | IUtilAction>
  ) => {
    const res = await fetchCreateUser(email, password);
    const result = await res.json();
    if (res.ok) {
      dispatch(loginSuccess());
      dispatch(loadToken(result.token));
      dispatch(SetModalContent("註冊成功"));
      dispatch(push("/report"));
    } else {
      dispatch(SetModalContent("帳號已被註冊,請重新輸入"));
    }
  };
}

export function changePasswordThunk(
  email: string,
  currentPassword: string,
  newPassword: string
) {
  return async (
    dispatch: Dispatch<IAuthAction | CallHistoryMethodAction | IUtilAction>
  ) => {
    const res = await fetchChangePassword(email, currentPassword, newPassword);
    const result = await res.json();
    if (res.ok) {
      dispatch(SetModalContent("更改密碼成功"));
    }
  };
}
