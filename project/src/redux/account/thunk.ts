import { Dispatch } from "react";
import { fetchGetReportedItem } from "../../api/account";
import { failed, GotReportedItems, IAccAction } from "./action";
import { ReportedItemState } from "./state";

export function getReportedItemThunk(userId: number) {
  return async (dispatch: Dispatch<IAccAction>) => {
    const res = await fetchGetReportedItem(userId);
    const result: any = await res.json();
    const items: ReportedItemState[] = result.items;

    if (res.ok) {
      dispatch(GotReportedItems(items));
    } else {
      dispatch(
        failed("@@Acc/GOT_REPORTED_ITEMS_FAILED", "fail getReportedItemThunk")
      );
    }
  };
}
