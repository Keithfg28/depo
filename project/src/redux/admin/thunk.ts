import { Dispatch } from "react";
import {
  fetchEditReportedItem,
  fetchGetPendingParkingItem,
  fetchGetPostedParkingItem,
  fetchGetUserById,
  fetchPostParkingItem,
  fetchRemoveParkingItem,
} from "../../api/admin";
import {
  failed,
  GotPendingParkingItems,
  GotPostedParkingItems,
  GotUserByUserId,
  IAdminAction,
} from "./actions";
import { ParkingItemState } from "./state";

export function getPostedParkingItemThunk() {
  return async (dispatch: Dispatch<IAdminAction>) => {
    const res = await fetchGetPostedParkingItem();
    const result: any = await res.json();
    const items: ParkingItemState[] = result.items;

    if (res.ok) {
      dispatch(GotPostedParkingItems(items));
    } else {
      dispatch(
        failed(
          "@@Admin/GOT_PARKING_ITEMS_FAILED",
          "fail getPostedParkingItemThunk"
        )
      );
    }
  };
}

export function getPendingParkingItemThunk() {
  return async (dispatch: Dispatch<IAdminAction>) => {
    const res = await fetchGetPendingParkingItem();
    const result: any = await res.json();
    const items: ParkingItemState[] = result.items;

    if (res.ok) {
      dispatch(GotPendingParkingItems(items));
    } else {
      dispatch(
        failed(
          "@@Admin/GOT_PARKING_ITEMS_FAILED",
          "fail getPendingParkingItemThunk"
        )
      );
    }
  };
}

export function removeParkingItemThunk(id: number) {
  return async (dispatch: Dispatch<IAdminAction>) => {
    const res = await fetchRemoveParkingItem(id);
    const result: any = await res.json();
    if (res.ok) {
    } else {
      dispatch(
        failed(
          "@@Admin/GOT_PARKING_ITEMS_FAILED",
          "fail removeParkingItemThunk"
        )
      );
    }
  };
}

export function postParkingItemThunk(id: number) {
  return async (dispatch: Dispatch<IAdminAction>) => {
    const res = await fetchPostParkingItem(id);
    const result: any = await res.json();

    if (res.ok) {
    } else {
      dispatch(
        failed("@@Admin/GOT_PARKING_ITEMS_FAILED", "fail postParkingItemThunk")
      );
    }
  };
}

export function editReportedItemThunk(userId: number, parkingItem: any) {
  return async (dispatch: Dispatch<IAdminAction>) => {
    const res = await fetchEditReportedItem(userId, parkingItem);
    const result = await res.json();

    if (res.ok) {
    } else {
      dispatch(
        failed("@@Admin/GOT_PARKING_ITEMS_FAILED", "fail editReportedItemThunk")
      );
    }
  };
}
