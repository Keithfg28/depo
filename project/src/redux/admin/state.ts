export interface IAdminState {
    parkingItems: ParkingItemState[],
    pendingItems: ParkingItemState[],
    selectedItemId: number | null,

}


export interface ParkingItemState {
    id: number
    place: string
    location: string
    hourly_rent: number;
    day_rent: number;
    night_rent: number;
    isIndoor: boolean;
    image: string;
    remark: string;
    users_id: number
    post: boolean;
}

