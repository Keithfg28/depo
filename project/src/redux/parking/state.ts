export interface ParkingItemState {
  id: number;
  key: number;
  photos: string;
  text: string;
  formatted_address: string;
  geometry: any;
  district: string;
  place: string;
  place_id: string;
  location: string;
  lat: number;
  lng: number;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  mapApi: any;
  mapApiLoaded: any;
  isIndoor: boolean;
  image: string;
  remark: string;
  users_id: number;
  post: boolean;
}

export interface IParkingState {
  parkingItems: ParkingItemState[];
  mapApi: any;
  mapApiLoaded: boolean;
  mapInstance: any;
}

export interface myPosition {
  lat: number;
  lng: number;
}
export interface defaultProps {
  center: {
    lat: number;
    lng: number;
  };
  zoom: number;
}

export interface markerDescriptionState {
  id: number;
  place: string;
  lat: number;
  lng: number;
  key: number;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  remark: string;
  updated_at: Date;
  photos: any;
  isIndoor: boolean;
  text: string;
  place_id: string;
  image: any;
  location: string;
}
export interface repairPlacesState {
  id: number;
  place: string;
  lat: number;
  lng: number;
  icon: string;
  key: number;
  hourly_rent: number;
  geometry: {
    location: {
      lat: () => number;
      lng: () => number;
    };
  };
  text: string;
  place_id: string;
  photos: any;
  formatted_address: string;
}
export interface parkingPlacesState {
  id: number;
  name: string;
  lat: number;
  lng: number;
  icon: string;
  key: number;
  hourlyRent: number;
  geometry: {
    location: {
      lat: () => number;
      lng: () => number;
    };
  };
  text: string;
  place_id: string;
  photos: any;
  formatted_address: string;
}

export interface repairMarkers {
  addListener(arg0: string, arg1: () => void): any;
  setLabel(arg0: { text: any; className: string; color: string }): any;
  setZIndex(arg0: number): number;
  position: {
    lat: number;
    lng: number;
  };
  icon: string;
  map: any;
  label: {};
}
export interface placeMarkers {
  addListener(arg0: string, arg1: () => void): any;
  setLabel(arg0: { text: any; className: string; color: string }): any;
  setZIndex(arg0: number): number;
  position: {
    lat: number;
    lng: number;
  };
  icon: string;
  map: any;
  label: {};
}
export const defaultProps: defaultProps = {
  center: {
    lat: 22.3174308,
    lng: 114.1646368,
  },
  zoom: 13,
};
