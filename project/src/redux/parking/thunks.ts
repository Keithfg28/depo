import { Dispatch } from "redux";
import { fetchGetPostedParkingItem } from "../../api/parking";
import { failed, GotPostedParkingItems, IParkingAction } from "./actions";
import { ParkingItemState } from "./state";

export function getPostedParkingItemThunk() {
  return async (dispatch: Dispatch<IParkingAction>) => {
    const res = await fetchGetPostedParkingItem();
    const result: any = await res.json();
    const items: ParkingItemState[] = result.items;

    if (res.ok) {
      dispatch(GotPostedParkingItems(items));
    } else {
      dispatch(
        failed("@@Parking/GOT_PARKING_ITEMS_FAILED", "fail getRepairItemThunk")
      );
    }
  };
}
