import { TypeOfTag } from "typescript";
import { ReportState } from "./state";


export function loadToken(token: string) {
    return {
        type: '@Auth/load_token' as const,
        token,
    }
}
export function getReportData(data: ReportState[]) {
    return {
        type: "@@Report/LOAD_REPORT_DATA" as const,
        data
    }
}

export function createReportData(data: ReportState[]) {
    return {
        type: "@@Report/CREATE_REPORT_DATA" as const,
        data
    }
}

type FAILED_INTENT = ""
    | "@@Report/GOT_REPORT_DATA_FAILED"


export function failed(type: FAILED_INTENT, msg: string) {
    return {
        type,
        msg
    }
}

export type IReportAction =
    | ReturnType<typeof getReportData>
    | ReturnType<typeof createReportData>
    | ReturnType<typeof failed>