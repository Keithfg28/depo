export function SetIsInfoModalClose() {
  return {
    type: "@@Util/SET_IS_INFO_MODAL_CLOSE" as const,
  };
}
export function SetModalContent(data: string) {
  return {
    type: "@@Util/SET_MODAL_CONTENT" as const,
    data,
  };
}
export type IUtilAction =
  | ReturnType<typeof SetIsInfoModalClose>
  | ReturnType<typeof SetModalContent>;
