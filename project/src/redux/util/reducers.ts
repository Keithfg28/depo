import { IUtilAction } from "./action";
import { IUtilState } from "./state";

const initialState: IUtilState = {
  isInfoModalShow: false,
  content: "",
};

export const utilReducers = (
  state: IUtilState = initialState,
  action: IUtilAction
): IUtilState => {
  switch (action.type) {
    case "@@Util/SET_IS_INFO_MODAL_CLOSE":
      return {
        ...state,
        isInfoModalShow: false,
      };
    case "@@Util/SET_MODAL_CONTENT":
      return {
        ...state,
        isInfoModalShow: true,
        content: action.data,
      };
    default:
      return state;
  }
};
