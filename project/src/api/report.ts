import { ReportState } from "../redux/report/state";

const { REACT_APP_API_SERVER } = process.env;

export async function fetchCreateReportData(data: ReportState) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/report`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      data,
    }),
  });
  return res;
}

export async function fetchGetReportData() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/report`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  return res;
}
