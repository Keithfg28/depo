const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetPostedParkingItem() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/admin/post`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchGetPendingParkingItem() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/admin/pending`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchRemoveParkingItem(itemId: number) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/admin/post`, {
    method: "DELETE",
    body: JSON.stringify({
      id: itemId,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchPostParkingItem(itemId: number) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/admin/pending`, {
    method: "PUT",
    body: JSON.stringify({
      id: itemId,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchEditReportedItem(itemId: number, parkingItem: any) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/admin/edit`, {
    method: "PUT",
    body: JSON.stringify({
      id: itemId,
      data: parkingItem,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchGetUserById(id: number) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/admin/getUser`, {
    method: "POST",
    body: JSON.stringify({
      id: id,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}
