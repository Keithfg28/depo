const { REACT_APP_API_SERVER } = process.env;

export async function fetchLogin(email: string, password: string) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      password,
    }),
  });
  return res;
}

export async function fetchGoogleLogin(accessToken: string) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/loginGoogle`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      accessToken,
    }),
  });
  return res;
}

export async function fetchFacebookLogin(accessToken: string) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/loginFacebook`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      accessToken,
    }),
  });
  return res;
}

export async function fetchCreateUser(email: string, password: string) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/createUser`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      password,
    }),
  });
  return res;
}

export async function fetchChangePassword(
  email: string,
  currentPassword: string,
  newPassword: string
) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/changePassword`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      currentPassword,
      newPassword,
    }),
  });
  return res;
}
